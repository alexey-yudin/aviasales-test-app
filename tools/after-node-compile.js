const path = require('path');
const ncp = require('ncp').ncp;

function copyData() {
  const source = path.resolve('src', 'shared', 'data');
  const destination = path.resolve('build', 'shared', 'data');
  ncp(source, destination, err => {
    if (err) {
      console.error(err);
    }
  });
}

copyData();
