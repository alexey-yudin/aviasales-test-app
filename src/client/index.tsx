import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './App';
import { Provider } from 'react-redux';
import { configureStore } from './store/configureStore';
import './styles/main.scss';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('app')
);
