import { applyMiddleware, createStore, Store } from 'redux';
import { AppInitialState, IAppState } from './appState';
import thunk from 'redux-thunk';
import { rootReducer } from './rootReducer';

export function configureStore(initialState: IAppState = AppInitialState): Store<IAppState> {
  return createStore(rootReducer, initialState, applyMiddleware(thunk));
}
