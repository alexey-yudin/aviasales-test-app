import { IAppState } from './appState';
import { combineReducers, Reducer } from 'redux';
import { ticketsReducer } from '../reducers/tickets';
import { currencyReducer } from '../reducers/currency';
import { drawerReducer } from '../reducers/drawer';

type Reducers = {
  [P in keyof IAppState]: Reducer<IAppState[P]>;
};

const reducers: Reducers = {
  ticketsState: ticketsReducer,
  currencyState: currencyReducer,
  drawerState: drawerReducer
};

export const rootReducer = combineReducers<IAppState>(reducers);
