import { ITicketsState, ticketsInitialState } from '../reducers/tickets';
import { currencyInitialState, ICurrencyState } from '../reducers/currency';
import { drawerInitialState, IDrawerState } from '../reducers/drawer';

export interface IAppState {
  ticketsState: ITicketsState;
  currencyState: ICurrencyState;
  drawerState: IDrawerState;
}

export const AppInitialState: IAppState = {
  ticketsState: ticketsInitialState,
  currencyState: currencyInitialState,
  drawerState: drawerInitialState
};
