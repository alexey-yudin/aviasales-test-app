import * as React from 'react';
import { Component } from 'react';
import { ETicketStop } from '../../reducers/tickets';

interface IStopsFilterCheckboxProps {
  text: string;
  isChecked: boolean;
  ticketStop: ETicketStop | null;

  onCheckboxChange(stop: ETicketStop): void;
  onUncheckOtherClick(stop: ETicketStop): void;
}

export class StopsFilterCheckbox extends Component<IStopsFilterCheckboxProps, {}> {
  render(): JSX.Element {
    const { isChecked, text } = this.props;

    return (
      <div className={'checkboxes-list__item'}>
        <label className={'checkboxes-list__label'}>
          <span className='checkbox'>
            <input
              type='checkbox'
              checked={isChecked}
              onChange={this.onCheckboxChange}
              className={'checkbox__input'}
            />
            <span className='checkbox__checkmark' />
          </span>
          {text}
        </label>
        {this.renderUnckeckOther()}
      </div>
    );
  }

  private renderUnckeckOther = (): JSX.Element => {
    return this.props.ticketStop != null && (
      <div className='checkboxes-list__additional'>
        <button
          type='button'
          className='checkboxes-list__uncheck-other'
          onClick={this.onUncheckOtherClick}
        >
          Только
        </button>
      </div>
    );
  };

  private onCheckboxChange = () => {
    const { ticketStop } = this.props;
    this.props.onCheckboxChange(ticketStop);
  };

  private onUncheckOtherClick = () => {
    const { ticketStop } = this.props;
    this.props.onUncheckOtherClick(ticketStop);
  };
}
