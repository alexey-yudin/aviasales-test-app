import * as React from 'react';
import { PureComponent } from 'react';
import { ECurrency } from '../../reducers/currency';
import { currencyConverter } from '../../helpers/currency-converter';
import { normalizeNum } from '../../helpers/normalize-num';
import { getFormattedDate } from '../../helpers/date-formatter';
import { priceFormatter } from '../../helpers/price-formatter';
import { ITicket } from '../../../shared/models/ticket';

interface ITicketListItemProps {
  ticket: ITicket;
  currency: ECurrency;
}

export class TicketListItem extends PureComponent<ITicketListItemProps, {}> {
  render(): JSX.Element {
    const { ticket, currency } = this.props;
    const formattedDepartureDate = getFormattedDate(ticket.departure_date);
    const formattedArrivalDate = getFormattedDate(ticket.arrival_date);
    const price = priceFormatter(currencyConverter(ticket.price, currency));

    return (
      <div className={'ticket'}>
        <div className='ticket__header'>
          <div className='ticket__airline-logo'>
            <img
              className='ticket__airline-logo__image'
              src={require('../../resources/img/airline-logo.png')}
              alt='logo'
            />
          </div>
          <button type='button' className='buy-button'>
            <span className='buy-button__text'>
              Купить
          </span>
            <span className='buy-button__price'>
              за {price} {this.renderCurrencySymbol()}
            </span>
          </button>
        </div>

        <div className='ticket__devider'/>

        <div className='ticket-content'>
          <div className='ticket-content__top'>
            <div className='ticket-content__time'>
              {this.normalizeTime(ticket.departure_time)}
            </div>
            <div className='ticket-stop'>
              <div className='ticket-stop__text'>
                {this.renderStopsText()}
              </div>
              <div className='ticket-stop__line'/>
            </div>
            <div className='ticket-content__time'>
              {this.normalizeTime(ticket.arrival_time)}
            </div>
          </div>

          <div className='ticket-content__bottom'>
            <div className='ticket-content__bottom-item ticket-content__origin'>
              <div className='ticket-content__route-name'>
                {ticket.origin}, {ticket.origin_name}
              </div>
              <div className='ticket-content__date'>
                {formattedDepartureDate.dayOfMonth}
                &nbsp;
                {formattedDepartureDate.month}
                &nbsp;
                {formattedDepartureDate.year},
                &nbsp;
                <span className='ticket-content__date__time'>
                  {formattedDepartureDate.dayOfWeek}
                </span>
              </div>
            </div>
            <div className='ticket-content__bottom-item ticket-content__destination'>
              <div className='ticket-content__route-name'>
                {ticket.destination_name}, {ticket.destination}
              </div>
              <div className='ticket-content__date'>
                {formattedArrivalDate.dayOfMonth}
                &nbsp;
                {formattedArrivalDate.month}
                &nbsp;
                {formattedArrivalDate.year},
                &nbsp;
                <span className='ticket-content__date__time'>
                  {formattedArrivalDate.dayOfWeek}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  private renderStopsText = (): string => {
    const { ticket } = this.props;
    if (ticket.stops === 0) {
      return null;
    }

    if (ticket.stops === 1) {
      return `${ticket.stops} пересадка`;
    } else {
      return `${ticket.stops} пересадки`;
    }
  };

  private normalizeTime = (time: string): string => {
    const [hours, minutes] = time.split(':');

    return `${normalizeNum(parseInt(hours))}:${normalizeNum(parseInt(minutes))}`;
  };

  private renderCurrencySymbol = (): string => {
    return {
      [ECurrency.EUR]: '€',
      [ECurrency.USD]: '$',
      [ECurrency.RUB]: '₽'
    }[this.props.currency];
  };
}
