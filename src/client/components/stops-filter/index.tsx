import * as React from 'react';
import { Component } from 'react';
import { ETicketStop } from '../../reducers/tickets';
import { StopsFilterCheckbox } from '../stops-filter-checkbox/stops-filter-checkbox';

export interface IStopsFilterStateProps {
  stops: ETicketStop[];
}

export interface IStopsFilterDispatchProps {
  setStops(stops: ETicketStop[]): void;
}

type TStopsFilterProps = IStopsFilterStateProps & IStopsFilterDispatchProps;

export class StopsFilter extends Component<TStopsFilterProps, {}> {
  private allStops = [
    ETicketStop.none,
    ETicketStop.one,
    ETicketStop.two,
    ETicketStop.three
  ];

  render(): JSX.Element {
    return (
      <div className={'stops-filter'}>
        <h4 className={'stops-filter__title'}>Количество пересадок</h4>

        <div className={'checkboxes-list'}>
          <StopsFilterCheckbox
            text={'Все'}
            isChecked={this.getAllCheckboxValue()}
            ticketStop={null}
            onCheckboxChange={this.onAllChange}
            onUncheckOtherClick={this.onUncheckOtherClick}
          />

          <StopsFilterCheckbox
            text={'Без пересадок'}
            isChecked={this.getCheckboxValue(ETicketStop.none)}
            ticketStop={ETicketStop.none}
            onCheckboxChange={this.onStopChange}
            onUncheckOtherClick={this.onUncheckOtherClick}
          />

          <StopsFilterCheckbox
            text={'1 пересадка'}
            isChecked={this.getCheckboxValue(ETicketStop.one)}
            ticketStop={ETicketStop.one}
            onCheckboxChange={this.onStopChange}
            onUncheckOtherClick={this.onUncheckOtherClick}
          />

          <StopsFilterCheckbox
            text={'2 пересадки'}
            isChecked={this.getCheckboxValue(ETicketStop.two)}
            ticketStop={ETicketStop.two}
            onCheckboxChange={this.onStopChange}
            onUncheckOtherClick={this.onUncheckOtherClick}
          />

          <StopsFilterCheckbox
            text={'3 пересадки'}
            isChecked={this.getCheckboxValue(ETicketStop.three)}
            ticketStop={ETicketStop.three}
            onCheckboxChange={this.onStopChange}
            onUncheckOtherClick={this.onUncheckOtherClick}
          />
        </div>
      </div>
    );
  }

  private onAllChange = (): void => {
    const { stops } = this.props;

    if (stops.length === this.allStops.length) {
      this.props.setStops([]);
    } else {
      this.props.setStops(this.allStops);
    }
  };

  private onStopChange = (ticketStop: ETicketStop): void => {
    const { stops } = this.props;

    if (stops.indexOf(ticketStop) > -1) {
      this.props.setStops(
        stops.filter(i => i !== ticketStop)
      );
    } else {
      this.props.setStops(
        stops.concat(ticketStop)
      );
    }
  };

  private onUncheckOtherClick = (stop: ETicketStop) => {
    this.props.setStops([stop]);
  };

  private getAllCheckboxValue(): boolean {
    const { stops } = this.props;

    return stops
      .filter((stop, index, self) => self.indexOf(stop) === index)
      .length === this.allStops.length;
  }

  private getCheckboxValue(stop: ETicketStop): boolean {
    const { stops } = this.props;

    return stops.indexOf(stop) > -1;
  }
}
