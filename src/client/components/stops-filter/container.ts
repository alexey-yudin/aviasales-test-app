import { connect } from 'react-redux';
import { IStopsFilterDispatchProps, IStopsFilterStateProps, StopsFilter } from './index';
import { TicketActions } from '../../actions/tickets';
import { ETicketStop } from '../../reducers/tickets';
import { Dispatch } from 'redux';
import { IAppState } from '../../store/appState';

export const StopsFilterContainer = connect<IStopsFilterStateProps, IStopsFilterDispatchProps>(
  (state: IAppState) => ({
    stops: state.ticketsState.stops
  }),
  (dispatch: Dispatch): IStopsFilterDispatchProps => ({
    setStops: (stops: ETicketStop[]) => dispatch(TicketActions.setStops(stops))
  })
)(StopsFilter);
