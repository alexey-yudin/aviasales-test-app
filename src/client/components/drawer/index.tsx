import * as React from 'react';
import { Component } from 'react';
import { StopsFilterContainer } from '../stops-filter/container';
import { EDrawerState } from '../../reducers/drawer';

export interface IDrawerStateProps {
  drawerState: EDrawerState;
}

export interface IDrawerDispatchProps {
  closeDrawer(): void;
}

type IDrawerProps = IDrawerStateProps & IDrawerDispatchProps;

export class Drawer extends Component<IDrawerProps, {}> {
  private drawerRef: HTMLDivElement;
  private backdropRef: HTMLDivElement;
  private drawerOpenClassName = 'drawer--opened';
  private backdropOpenClassName = 'drawer__backdrop--opened';

  componentWillReceiveProps(nextProps: IDrawerStateProps) {
    if (nextProps.drawerState === EDrawerState.opened) {
      this.onDrawerOpen();
    } else if (nextProps.drawerState === EDrawerState.closed) {
      this.onDrawerClose();
    }
  }

  render(): JSX.Element {
    return (
      <>
        <div className='drawer' ref={ref => this.drawerRef = ref}>
          <div className='drawer__content'>
            <StopsFilterContainer/>
          </div>
        </div>
        <div
          ref={ref => this.backdropRef = ref}
          onClick={this.closeDrawer}
          className='drawer__backdrop'
        />
      </>
    );
  }

  private onDrawerOpen = () => {
    this.drawerRef.classList.add(this.drawerOpenClassName);
    this.backdropRef.classList.add(this.backdropOpenClassName);
  };

  private closeDrawer = () => {
    this.props.closeDrawer();
  };

  private onDrawerClose = () => {
    this.drawerRef.classList.remove(this.drawerOpenClassName);
    this.backdropRef.classList.remove(this.backdropOpenClassName);
  };
}
