import { connect } from 'react-redux';
import { Drawer, IDrawerDispatchProps, IDrawerStateProps } from './index';
import { IAppState } from '../../store/appState';
import { Dispatch } from 'redux';
import { DrawerActions } from '../../actions/drawer';

export const DrawerContainer = connect<IDrawerStateProps, IDrawerDispatchProps>(
  (state: IAppState) => ({
    drawerState: state.drawerState.drawerState
  }),
  (dispatch: Dispatch) => ({
    closeDrawer: () => dispatch(DrawerActions.closeDrawer())
  })
)(Drawer);
