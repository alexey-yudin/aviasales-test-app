import * as React from 'react';
import { Component } from 'react';
import { EDrawerState } from '../../reducers/drawer';

export interface IDrawerButtonStateProps {
  drawerState: EDrawerState;
}

export interface IDrawerButtonDistatchProps {
  openDrawer(): void;

  closeDrawer(): void;
}

type IDrawerButtonProps = IDrawerButtonStateProps & IDrawerButtonDistatchProps;

export class DrawerButton extends Component<IDrawerButtonProps, {}> {
  render(): JSX.Element {
    return (
      <>
        {this.renderButton()}
      </>
    );
  }

  private renderButton = (): JSX.Element => {
    if (this.props.drawerState === EDrawerState.closed) {
      return (
        <button
          type='button'
          className='drawer-button drawer-button--open'
          onClick={this.openDrawer}
        />
      );
    } else if (this.props.drawerState === EDrawerState.opened) {
      return (
        <button
          type='button'
          className='drawer-button drawer-button--close'
          onClick={this.closeDrawer}
        />
      );
    }
  };

  private openDrawer = () => {
    this.props.openDrawer();
  };

  private closeDrawer = () => this.props.closeDrawer();
}
