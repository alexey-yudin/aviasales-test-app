import { connect } from 'react-redux';
import { DrawerButton, IDrawerButtonDistatchProps, IDrawerButtonStateProps } from './index';
import { IAppState } from '../../store/appState';
import { Dispatch } from 'redux';
import { DrawerActions } from '../../actions/drawer';

export const DrawerButtonContainer = connect<IDrawerButtonStateProps, IDrawerButtonDistatchProps>(
  (state: IAppState) => ({
    drawerState: state.drawerState.drawerState
  }),
  (dispatch: Dispatch) => ({
    openDrawer: () => dispatch(DrawerActions.openDrawer()),
    closeDrawer: () => dispatch(DrawerActions.closeDrawer())
  })
)(DrawerButton);
