import { connect } from 'react-redux';
import { CurrencySelector, ICurrencySelectorDispatchProps, ICurrencySelectorStateProps } from './index';
import { IAppState } from '../../store/appState';
import { Dispatch } from 'redux';
import { ECurrency } from '../../reducers/currency';
import { CurrencyActions } from '../../actions/currency';

export const CurrencySelectorContainer = connect<ICurrencySelectorStateProps, ICurrencySelectorDispatchProps>(
  (state: IAppState) => ({
    currency: state.currencyState.currency
  }),
  (dispatch: Dispatch): ICurrencySelectorDispatchProps => ({
    setCurrency: (currency: ECurrency) => dispatch(CurrencyActions.setCurrency(currency))
  })
)(CurrencySelector);
