import * as React from 'react';
import { Component } from 'react';
import { ECurrency } from '../../reducers/currency';

export interface ICurrencySelectorStateProps {
  currency: ECurrency;
}

export interface ICurrencySelectorDispatchProps {
  setCurrency(currency: ECurrency): void;
}

type TCurrencySelectorProps = ICurrencySelectorStateProps & ICurrencySelectorDispatchProps;

export class CurrencySelector extends Component<TCurrencySelectorProps, {}> {
  render(): JSX.Element {
    return (
      <div className='currency-controls'>
        <h4 className='currency-controls__title'>Валюта</h4>
        <div className='currency-controls__buttons'>
          <button
            type='button'
            onClick={this.setRUBCurrency}
            className={`currency-button ${this.getActiveButtonStyles(ECurrency.RUB)}`}
          >
            RUB
          </button>
          <button
            type='button'
            onClick={this.setUSDCurrency}
            className={`currency-button ${this.getActiveButtonStyles(ECurrency.USD)}`}
          >
            USD
          </button>
          <button
            type='button'
            onClick={this.setEURCurrency}
            className={`currency-button ${this.getActiveButtonStyles(ECurrency.EUR)}`}
          >
            EUR
          </button>
        </div>
      </div>
    );
  }

  private getActiveButtonStyles = (currency: ECurrency): string | void =>
    this.isActive(currency) ? 'currency-button--active' : '';

  private isActive = (currency: ECurrency): boolean => this.props.currency === currency;

  private setRUBCurrency = (): void => this.props.setCurrency(ECurrency.RUB);

  private setUSDCurrency = (): void => this.props.setCurrency(ECurrency.USD);

  private setEURCurrency = (): void => this.props.setCurrency(ECurrency.EUR);
}
