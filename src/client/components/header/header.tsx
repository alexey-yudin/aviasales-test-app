import * as React from 'react';

export const Header = () => {
  return (
    <div className='header'>
      <a href='/'>
        <img src={require('../../resources/img/logo.svg')} alt='logo'/>
      </a>
    </div>
  );
};
