import * as React from 'react';
import { Component } from 'react';
import * as uuid from 'uuid';
import { ECurrency } from '../../reducers/currency';
import { ITicket } from '../../../shared/models/ticket';
import { TicketListItem } from '../ticket-list-item';

export interface ITicketListStateProps {
  tickets: ITicket[];
  currency: ECurrency;
}

export interface ITicketListDispatchProps {
  getTickets(): void;
}

type ITicketListProps = ITicketListStateProps & ITicketListDispatchProps;

export class TicketList extends Component<ITicketListProps, {}> {
  componentWillMount() {
    this.props.getTickets();
  }

  render(): JSX.Element {
    const { tickets } = this.props;

    return (
      <div className={'tickets-list'}>
        {tickets.map(this.renderTicket)}
      </div>
    );
  }

  private renderTicket = (ticket: ITicket): JSX.Element => {
    const { currency } = this.props;

    return (
      <TicketListItem
        key={uuid()}
        ticket={ticket}
        currency={currency}
      />
    );
  };
}
