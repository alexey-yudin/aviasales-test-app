import { connect } from 'react-redux';
import { IAppState } from '../../store/appState';
import { ITicketListDispatchProps, ITicketListStateProps, TicketList } from './index';
import { TicketsActionsAsync } from '../../actions/tickets-async';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { getTicketsFilteredByStops } from '../../selectors/tickets';

export const TicketListContainer = connect<ITicketListStateProps, ITicketListDispatchProps>(
  (state: IAppState) => ({
    tickets: getTicketsFilteredByStops(state),
    currency: state.currencyState.currency
  }),
  (dispatch: ThunkDispatch<void, IAppState, Action>) => ({
    getTickets: () => dispatch(TicketsActionsAsync.getTickets())
  })
)(TicketList);
