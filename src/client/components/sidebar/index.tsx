import * as React from 'react';
import { CurrencySelectorContainer } from '../currency-selector/container';
import { StopsFilterContainer } from '../stops-filter/container';

export const Sidebar = () => {
  return (
    <div className='sidebar__container'>
      <CurrencySelectorContainer/>
      <StopsFilterContainer/>
    </div>
  );
};
