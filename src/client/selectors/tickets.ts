import { IAppState } from '../store/appState';
import { ITicket } from '../../shared/models/ticket';

export function getTicketsFilteredByStops(state: IAppState): ITicket[] {
  const { ticketsState: { tickets, stops } } = state;

  return tickets.filter(ticket => stops.indexOf(ticket.stops) > -1);
}
