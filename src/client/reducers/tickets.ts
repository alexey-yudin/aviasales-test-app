import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { TicketActions } from '../actions/tickets';
import { ITicket } from '../../shared/models/ticket';
import { Success } from 'typescript-fsa';

export interface ITicketsState {
  tickets: ITicket[];
  stops: ETicketStop[];
}

export enum ETicketStop {
  none = 0,
  one = 1,
  two = 2,
  three = 3
}

export const ticketsInitialState: ITicketsState = {
  tickets: [],
  stops: [
    ETicketStop.none,
    ETicketStop.one,
    ETicketStop.two,
    ETicketStop.three
  ]
};

function getTicketsDoneHandler(state: ITicketsState, success: Success<{}, ITicket[]>): ITicketsState {
  return { ...state, tickets: success.result };
}

function setStopsHandler(state: ITicketsState, stops: ETicketStop[]): ITicketsState {
  return { ...state, stops };
}

export const ticketsReducer = reducerWithInitialState(ticketsInitialState)
  .case(TicketActions.getTickets.done, getTicketsDoneHandler)
  .case(TicketActions.setStops, setStopsHandler);
