import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { DrawerActions } from '../actions/drawer';

export interface IDrawerState {
  drawerState: EDrawerState;
}

export enum EDrawerState {
  opened = 'opened',
  closed = 'closed'
}

export const drawerInitialState: IDrawerState = {
  drawerState: EDrawerState.closed
};

function openDrawer(state: IDrawerState): IDrawerState {
  return { ...state, drawerState: EDrawerState.opened };
}

function closeDrawer(state: IDrawerState): IDrawerState {
  return { ...state, drawerState: EDrawerState.closed };
}

export const drawerReducer = reducerWithInitialState(drawerInitialState)
  .case(DrawerActions.openDrawer, openDrawer)
  .case(DrawerActions.closeDrawer, closeDrawer);
