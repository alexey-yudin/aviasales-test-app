import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { CurrencyActions } from '../actions/currency';

export enum ECurrency {
  RUB = 'RUB',
  EUR = 'EUR',
  USD = 'USD'
}

export interface ICurrencyState {
  currency: ECurrency;
}

export const currencyInitialState: ICurrencyState = {
  currency: ECurrency.RUB
};

function setCurrencyHandler(state: ICurrencyState, currency: ECurrency): ICurrencyState {
  return { ...state, currency };
}

export const currencyReducer = reducerWithInitialState(currencyInitialState)
  .case(CurrencyActions.setCurrency, setCurrencyHandler);
