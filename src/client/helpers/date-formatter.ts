import * as moment from 'moment';

moment.locale('ru');

export function getFormattedDate(date: string): IFormattedDate {
  const [month, day, year] = date.split('.');
  const newDate = new Date(parseInt(`20${year}`, 10), parseInt(month, 10) - 1, parseInt(day));

  const formattedDate = moment(newDate).format('D MMM YYYY, dd')
    .split('.').join(' ').split(',').join(' ').split(' ').filter(i => i !== '');

  return {
    dayOfMonth: formattedDate[0],
    month: formattedDate[1],
    year: formattedDate[2],
    dayOfWeek: formattedDate[3]
  };
}

export interface IFormattedDate {
  dayOfMonth: string;
  month: string;
  year: string;
  dayOfWeek: string;
}
