import { ECurrency } from '../reducers/currency';

export const currencyRates = {
  [ECurrency.EUR]: 72.63,
  [ECurrency.USD]: 60.13
};

export function currencyConverter(price: number, currency: ECurrency): number {
  if (currency === ECurrency.RUB) {
    return price;
  }

  if (price <= 0) {
    return price;
  }

  const result = price / currencyRates[currency];

  return Math.round(result * 100) / 100;
}
