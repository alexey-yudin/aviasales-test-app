export function priceFormatter(currency: number): string {
  if (currency == null) {
    return '';
  }
  const numberTriads = [];
  const splittedPrice = currency.toString().split('.');
  const price = splittedPrice[0];
  const fraction = splittedPrice[1];

  for (let i = 0; i < price.length; i++) {
    if (i % 3 === 0) {
      numberTriads.push(price.substr(price.length - i, 3));
    }
  }

  const numberTriadsStr = numberTriads.reverse().join('');
  if (numberTriadsStr.length !== price.length) {
    const diff = price.length - numberTriadsStr.length;
    numberTriads.unshift(price.substr(0, diff));
  }

  const formattedPrice = numberTriads.filter(item => item.length > 0).join(' ');
  if (fraction != null) {
    return `${formattedPrice}.${fraction}`;
  } else {
    return `${formattedPrice}`;
  }
}
