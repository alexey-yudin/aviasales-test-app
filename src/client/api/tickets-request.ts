import { ITicket } from '../../shared/models/ticket';
import { BaseRequest } from './base-request';

class TicketsRequest extends BaseRequest {
  getTickets(): Promise<ITicket[]> {
    return this.fetch('/api/tickets/get-tickets');
  }
}

export const ticketsRequest = new TicketsRequest();
