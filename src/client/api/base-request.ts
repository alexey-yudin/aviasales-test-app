export class BaseRequest {
  protected fetch(url: string): Promise<any> {
    return fetch(url).then(res => res.json());
  }
}
