import * as React from 'react';
import { Component } from 'react';
import { TicketListContainer } from './components/ticket-list/container';
import { Header } from './components/header/header';
import { Sidebar } from './components/sidebar';
import { CurrencySelectorContainer } from './components/currency-selector/container';
import { DrawerContainer } from './components/drawer/container';
import { DrawerButtonContainer } from './components/drawer-button/container';

export class App extends Component {
  render(): JSX.Element {
    return (
      <>
        <div className='wrapper'>
          <Header/>
          <div className='content'>
            <div className='sidebar'>
              <Sidebar/>
            </div>
            <div className='content-top-filters'>
              <CurrencySelectorContainer/>
              <DrawerButtonContainer/>
            </div>
            <div className='tickets'>
              <TicketListContainer/>
            </div>
          </div>
        </div>
        <DrawerContainer/>
      </>
    );
  }
}
