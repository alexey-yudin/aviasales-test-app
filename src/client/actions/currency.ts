import actionCreatorFactory from 'typescript-fsa';
import { ECurrency } from '../reducers/currency';

const actionCreator = actionCreatorFactory();

export class CurrencyActions {
  static setCurrency = actionCreator<ECurrency>('currency/SET_CURRENCY');
}
