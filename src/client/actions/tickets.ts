import actionCreatorFactory from 'typescript-fsa';
import { ITicket } from '../../shared/models/ticket';
import { ETicketStop } from '../reducers/tickets';

const actionCreator = actionCreatorFactory();

export class TicketActions {
  static getTickets = actionCreator.async<{}, ITicket[], Error>('tickets/GET_TICKETS');
  static setStops = actionCreator<ETicketStop[]>('tickets/SET_STOPS');
}
