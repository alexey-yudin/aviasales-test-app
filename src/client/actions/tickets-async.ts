import { TicketActions } from './tickets';
import { Dispatch } from 'redux';
import { ticketsRequest } from '../api/tickets-request';

export class TicketsActionsAsync {
  static getTickets() {
    return (dispatch: Dispatch) => {
      dispatch(TicketActions.getTickets.started({}));
      ticketsRequest.getTickets()
        .then(tickets => {
          dispatch(TicketActions.getTickets.done({ result: tickets, params: {} }));
        })
        .catch(error => dispatch(TicketActions.getTickets.failed({ params: {}, error })));
    };
  }
}
