import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory();

export class DrawerActions {
  static openDrawer = actionCreator('drawer/OPEN_DRAWER');
  static closeDrawer = actionCreator('drawer/CLOSE_DRAWER');
}
