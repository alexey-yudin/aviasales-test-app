import { Request, Response } from 'express';
import { ITicket } from '../../shared/models/ticket';

const tickets = require('../../shared/data/tickets.json');

export class TicketsController {
  static getTickets(req: Request, res: Response) {
    res.json(tickets.tickets as ITicket[]);
  }
}
