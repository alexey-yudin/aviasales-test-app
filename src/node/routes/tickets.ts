import { TicketsController } from '../controllers/tickets';
import * as express from 'express';

const router = express.Router();

router.get('/get-tickets', TicketsController.getTickets);

export = router;
