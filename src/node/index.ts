import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as ticketRoutes from './routes/tickets';

const port = process.env.PORT || 8000;
const app = express();

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, '/../client')));

app.use('/api/tickets', ticketRoutes);

app.listen(port, () => {
  console.info(`Listening on port: ${port}`);
});
