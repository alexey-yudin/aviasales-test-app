import { currencyConverter } from '../../src/client/helpers/currency-converter';
import { ECurrency } from '../../src/client/reducers/currency';

describe('Currency converter', () => {
  it('should not convert for RUB', () => {
    const price = 22345;
    const currency = ECurrency.RUB;
    expect(currencyConverter(price, currency)).toEqual(price);
  });

  it('should return the price if the price was  0', () => {
    const price = 0;
    expect(currencyConverter(price, ECurrency.USD)).toEqual(price);
  });

  it('should return the price if the price was negative or 0', () => {
    const price = -45345;
    expect(currencyConverter(price, ECurrency.USD)).toEqual(price);
  });

  it('should convert the value from RUB to EUR', () => {
    const price = 22345;
    const currency = ECurrency.EUR;
    const expectedResult = 307.66;
    expect(currencyConverter(price, currency)).toEqual(expectedResult);
  });

  it('should convert the value from RUB to USD', () => {
    const price = 22345;
    const currency = ECurrency.USD;
    const expectedResult = 371.61;
    expect(currencyConverter(price, currency)).toEqual(expectedResult);
  });
});
