import { ETicketStop, ITicketsState, ticketsReducer } from '../../src/client/reducers/tickets';
import { ticketsTestData } from './tickets-test-data';
import { TicketActions } from '../../src/client/actions/tickets';

describe('Tickets reducer', () => {
  const initialState: ITicketsState = {
    tickets: ticketsTestData.tickets,
    stops: []
  };

  describe('setStops', () => {
    it('should set stops', () => {
      const newState = ticketsReducer(initialState, TicketActions.setStops([ETicketStop.one]));
      expect(newState.stops.length).toEqual(1);
      expect(newState.stops[0]).toEqual(ETicketStop.one);
    });
  });
});
