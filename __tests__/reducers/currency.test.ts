import { currencyReducer, ECurrency, ICurrencyState } from '../../src/client/reducers/currency';
import { CurrencyActions } from '../../src/client/actions/currency';

describe('Currency reducer', function () {
  const initialState: ICurrencyState = {
    currency: ECurrency.RUB
  };

  describe('setCurrency', () => {
    it('should set USD currency', () => {
      const newState = currencyReducer(initialState, CurrencyActions.setCurrency(ECurrency.USD));
      expect(newState.currency).toEqual(ECurrency.USD);
    });

    it('should set EUR currency', () => {
      const newState = currencyReducer(initialState, CurrencyActions.setCurrency(ECurrency.EUR));
      expect(newState.currency).toEqual(ECurrency.EUR);
    });

    it('should set RUB currency', () => {
      const newState = currencyReducer(initialState, CurrencyActions.setCurrency(ECurrency.RUB));
      expect(newState.currency).toEqual(ECurrency.RUB);
    });
  });
});
