import * as React from 'react';
import * as Enzyme from 'enzyme';
import { shallow } from 'enzyme';
import * as EnzymeAdapter from 'enzyme-adapter-react-16';
import { ECurrency } from '../../src/client/reducers/currency';
import { currencyConverter } from '../../src/client/helpers/currency-converter';
import { priceFormatter } from '../../src/client/helpers/price-formatter';
import { ITicket } from '../../src/shared/models/ticket';
import { TicketListItem } from '../../src/client/components/ticket-list-item';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('TicketListItem component', () => {
  const ticket: ITicket = {
    'origin': 'VVO',
    'origin_name': 'Владивосток',
    'destination': 'TLV',
    'destination_name': 'Тель-Авив',
    'departure_date': '12.05.18',
    'departure_time': '16:20',
    'arrival_date': '12.05.18',
    'arrival_time': '22:10',
    'carrier': 'TK',
    'stops': 3,
    'price': 12400
  };

  describe('Currency convert', () => {
    it('should display the RUB value', () => {
      const ticketListItem = shallow(<TicketListItem ticket={ticket} currency={ECurrency.RUB}/>);
      const currencyElem = ticketListItem.find('.buy-button__price');
      const expectedResult = priceFormatter(ticket.price);
      expect(currencyElem.text().trim().indexOf(expectedResult) > -1).toBeTruthy();
    });

    it('should display the converted value for USD', () => {
      const ticketListItem = shallow(<TicketListItem ticket={ticket} currency={ECurrency.USD}/>);
      const currencyElem = ticketListItem.find('.buy-button__price');
      const expectedResult = priceFormatter(currencyConverter(ticket.price, ECurrency.USD));
      expect(currencyElem.text().trim().indexOf(expectedResult) > -1).toBeTruthy();
    });

    it('should display the converted value for EUR', () => {
      const ticketListItem = shallow(<TicketListItem ticket={ticket} currency={ECurrency.EUR}/>);
      const currencyElem = ticketListItem.find('.buy-button__price');
      const expectedResult = priceFormatter(currencyConverter(ticket.price, ECurrency.EUR));
      expect(currencyElem.text().trim().indexOf(expectedResult) > -1).toBeTruthy();
    });
  });
});
